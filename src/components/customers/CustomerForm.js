import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { postApi, putApi } from '../../commons/callApi'
import { CUSTOMER } from './const'

class CustomerForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      customer: props.customer
    }
  }

  static getDerivedStateFromProps(nextProps, prevState){
    if (nextProps.customer.id !== prevState.customer.id) {
      return { customer: nextProps.customer}
    }
    else return null
 }

  submitCustomer = () => {
    const { customer, handleCustomerForm, initialPage } = this.props
    
    if (customer.id) {
      putApi(`/customers/${customer.id}`, JSON.stringify({...this.state}))
      .then(response => {
        if (response.status === "success") {
          handleCustomerForm(false, true, initialPage)
        } else {
          console.log("fail")
        }
      })
    } else {
      postApi("/customers", JSON.stringify({...this.state}))
      .then(response => {
        if (response.status === "success") {
          new Promise((resolve) => {
            this.setState({
              customer: {...Object.assign({}, CUSTOMER)}
            })
            resolve()
          }).then( handleCustomerForm(false, true, 1) )
        } else {
          console.log("fail")
        }
      })
    }
  }

  handleChangeForm = (_field, _value) => {
    this.setState({
      customer: Object.assign({}, this.state.customer, {
        [_field]: _value
      })
    })
  }

  render() {
    const { toggleCustomerForm, handleCustomerForm, initialPage } = this.props
    let customer = Object.assign({}, this.state.customer)

    return (
      <Modal show={toggleCustomerForm}>
        <Modal.Header>
          <Modal.Title>Customer Form</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <form className="form-horizontal">
              <div className="form-group">
                <label className="control-label col-sm-2">First Name</label>
                <div className="col-sm-3">
                  <input
                    value={customer.firstName}
                    onChange={(e) => this.handleChangeForm('firstName', e.target.value)}
                    name="firstName"
                    type="text"
                    className="form-control"/>
                </div>
              </div>

              <div className="form-group">
                <label className="control-label col-sm-2">Last Name</label>
                <div className="col-sm-3">
                  <input
                    value={customer.lastName}
                    onChange={(e) => this.handleChangeForm('lastName', e.target.value)}
                    name="lastName"
                    type="text"
                    className="form-control"/>
                </div>
              </div>
              
              <div className="form-group">
                <label className="control-label col-sm-2">Email</label>
                <div className="col-sm-3">
                  <input
                    value={customer.email}
                    onChange={(e) => this.handleChangeForm('email', e.target.value)}
                    name="email" 
                    type="email"
                    className="form-control"/>
                </div>
              </div>

              <div className="form-group">
                <label className="control-label col-sm-2">Phone</label>
                <div className="col-sm-3">
                  <input
                    value={customer.phone}
                    onChange={(e) => this.handleChangeForm('phone', e.target.value)}
                    name="phone" 
                    type="text" 
                    className="form-control"/>
                </div>
              </div>

              <div className="form-group">
                <label className="control-label col-sm-2">Avatar</label>
                <div className="col-sm-3">
                  <input
                    value={customer.avatarUrl}
                    onChange={(e) => this.handleChangeForm('avatarUrl', e.target.value)}
                    name="avatarUrl" 
                    type="text" 
                    className="form-control"/>
                </div>
              </div>
            </form>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => handleCustomerForm(false, false, initialPage, CUSTOMER)}>Close</Button>
          <Button onClick={this.submitCustomer}>Save</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}

CustomerForm.propTypes = {
  customer: PropTypes.shape({
    lastName: PropTypes.string,
    firstName: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    avatarUrl: PropTypes.string,
  })
}

CustomerForm.defaultProps = {
  customer: Object.assign({}, CUSTOMER)
}

export default CustomerForm