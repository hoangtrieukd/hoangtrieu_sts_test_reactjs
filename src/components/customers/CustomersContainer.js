import React, { Component, Fragment } from 'react'
import ReactPaginate from 'react-paginate'
import CustomerRow from './CustomerRow'
import { getApi, deleteApi } from '../../commons/callApi'
import CustomerForm from './CustomerForm'

import { CUSTOMER } from './const'

class CustomersContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      customers: [],
      totalPages: null,
      initialPage: 1,
      toggleCustomerForm: false
    }
  }

  fetchCustomers = (_query, _initialPage = null) => {
    getApi(_query)
      .then(response =>
      {
        this.setState({
          customers: response.data,
          totalPages: response.totalPages,
          toggleCustomerForm: false,
          initialPage: _initialPage
        })
      }
    )
  }

  componentDidMount() {
    this.fetchCustomers("/customers?order=id DESC", 1)
  }

  handlePageClick = (_page) => {
    this.fetchCustomers(`/customers?order=id DESC&page=${_page.selected+1}`, _page.selected+1)
  }

  handleDeleteCustomer = (_id) => {
    let { customers, initialPage } = this.state
    deleteApi(`/customers/${_id}`)
      .then(response => {
        if (response.status === 204) {
          initialPage = customers.length === 1 ? initialPage -= 1 : initialPage
          this.fetchCustomers(`/customers?order=id DESC&page=${initialPage}`, initialPage)
        } else {
          console.log("fail")
        }
      })
  }

  handleCustomerForm = (_toggle, _isFetch = false, _initialPage, _customer = null) => {
    if (_isFetch) {
      this.fetchCustomers(`/customers?order=id DESC&page=${_initialPage}`, _initialPage)
    } else {
      this.setState({
        toggleCustomerForm: _toggle,
        customer: _customer
      })
    }
  }

  generateCustomerList = (_list) => {
    let { totalPages, initialPage } = this.state
    let result = []
    _list.forEach((customer) => {
      result.push(
        <CustomerRow
          key={`customer-${customer.id}`}
          initialPage={initialPage}
          handleCustomerForm={this.handleCustomerForm}
          handleDeleteCustomer={this.handleDeleteCustomer}
          customer={customer} />
      )
    })

    return (
      <Fragment>
        <table className="table table-border table-striped">
          <thead>
            <tr>
              <th className="text-center sort sorting">First Name</th>
              <th className="text-center">Last Name</th>
              <th className="text-center">Email</th>
              <th className="text-center">Phone</th>
              <th className="text-center">Avatar</th>
              <th className="text-center">
                <button type="button"
                        onClick={() => this.handleCustomerForm(true, false, initialPage, CUSTOMER)}
                        className="btn btn-primary"><i className="fa fa-plus-square" />
                </button>
              </th>
            </tr>
          </thead>

          <tbody>
            {result}
          </tbody>
        </table>

        <div>

        </div>

        <ReactPaginate previousLabel="previous"
                       nextLabel="next"
                       breakLabel={<a href="">...</a>}
                       breakClassName="break-me"
                       pageCount={totalPages}
                       marginPagesDisplayed={2}
                       pageRangeDisplayed={5}
                       onPageChange={this.handlePageClick}
                       containerClassName="pagination paginate-center"
                       subContainerClassName="pages pagination"
                       pageClassName="page-item"
                       previousLinkClassName="page-link"
                       nextLinkClassName="page-link"
                       pageLinkClassName="page-link"
                       activeClassName="active" />
      </Fragment>
    )
  }

  render() {
    let { customers, customer, toggleCustomerForm, initialPage } = this.state

    return (
      <div>
        {
          customers.length === 0 && <div>Loading</div>
        }
        {
          customers.length !== 0 && this.generateCustomerList(customers)
        }
        <CustomerForm    
          toggleCustomerForm={toggleCustomerForm}
          initialPage={initialPage}
          customer={customer}
          handleCustomerForm={this.handleCustomerForm} />
      </div>
    )
  }
}

export default CustomersContainer