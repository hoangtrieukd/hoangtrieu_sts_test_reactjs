import React, { Component } from 'react'

class CustomerRow extends Component {
  render() {
    let { customer, handleCustomerForm, initialPage, handleDeleteCustomer } = this.props

    return (
      <tr>
        <td>{customer.firstName}</td>
        <td>{customer.lastName}</td>
        <td>{customer.email}</td>
        <td>{customer.phone}</td>
        <td>{customer.avatarUrl}</td>
        <td>
        <div className="btn-group" role="group">
          <button 
            type="button" 
            onClick={() => handleCustomerForm(true, false, initialPage, customer)}
            className="btn btn-secondary"><i className="fa fa-edit" />
          </button>

          <button 
            type="button"
            onClick={() => handleDeleteCustomer(customer.id)}
            className="btn btn-secondary"><i className="fa fa-trash" />
          </button>
        </div>
        </td>
      </tr>
    )
  }
}

export default CustomerRow