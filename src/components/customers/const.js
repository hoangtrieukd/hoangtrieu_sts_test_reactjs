export const CUSTOMER = {
  id: null,
  lastName: '',
  firstName: '',
  email: '',
  phone: '',
  avatarUrl: ''
}