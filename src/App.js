import React, { Component } from 'react'
import CustomersContainer from './components/customers/CustomersContainer.js'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Customer Management</h1>
        </header>
        <CustomersContainer />
      </div>
    );
  }
}

export default App
