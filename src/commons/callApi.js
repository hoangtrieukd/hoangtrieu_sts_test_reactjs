const serverURL = "http://localhost:3000/api/v1"

export function getApi(_url) {
  return fetch(`${serverURL}${_url}`).then(response => response.json())
}

export function postApi(_url, _data) {
  return fetch(`${serverURL}${_url}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: _data
  }).then(response => response.json())
}

export function putApi(_url, _data) {
  return fetch(`${serverURL}${_url}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: _data
  }).then(response => response.json())
}

export function deleteApi(_url) {
  return fetch(`${serverURL}${_url}`, {
    method: "DELETE"
  })
}